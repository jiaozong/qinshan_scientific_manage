//import { talentRouter, constantRoutes } from '@/router';

const state = {
	/**第一步 盘点基本信息 */
	baseInfo: {},
	/**第二步 盘点范围 */
	rangeInfo: {},
	/**第三步 盘点规则 */
	rule: {},
};

const mutations = {
	setBaseInfo: (state, payload) => {
		state.baseInfo = payload;
	},
	setRangeInfo: (state, payload) => {
		console.log('111---rangeInfo---', state.rangeInfo);
		state.rangeInfo = payload;
	},
};

const actions = {};
const getters = {
	baseInfo: (state) => state.baseInfo,
	rangeInfo: (state) => state.rangeInfo,
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters,
};
