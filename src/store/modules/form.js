const getDefaultState = ()=>{
    console.log(123);
    return {
        isCollapse: false,
        tempObject: {}
    }
}
const state = getDefaultState();
const getters = {
    isCollapse: state => {
        return state.isCollapse
    },
    tempObject: state => {
        return state.tempObject
    }
}

const actions = {
    setIsCollapse(context, payload) {
        return new Promise(resolve => {
            // 模拟登录成功，写入 token 信息
            context.commit('setIsCollapse', payload)
            resolve()
        })
    }
}

const mutations = {
    UPDATE_STATE(state, data) {
        for (const key in data) {
            if (!data.hasOwnProperty(key)) { return }
            if (data[key] instanceof Object) {
                const tdata = data[key];
                for (const tkey in tdata) {
                    state[key][tkey] = tdata[tkey]
                }
            } else {
                state[key] = data[key]
            }

        }
    },
    setIsCollapse(state, status) {
        state.isCollapse = status
    }
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}
