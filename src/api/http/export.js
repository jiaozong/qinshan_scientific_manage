import axios from 'axios';
import { getLocalStorage, removeLocalStorage } from '@/utils/cache';

export default function exportExcel(url, params, name) {
	return new Promise((resolve) => {
		axios.defaults.headers.common['token'] = getLocalStorage('token');
		axios.defaults.headers.common['Content-Type'] = 'multipart/form-data';
		axios.defaults.responseType = 'arraybuffer';
		axios
			.get(url, params)
			.then((res) => {
				const blob = new Blob([res.data], {
					type: 'application/vnd.ms-excel',
				});
				if ('download' in document.createElement('a')) {
					const emlink = document.createElement('a');
					emlink.style.display = 'none';
					emlink.href = URL.createObjectURL(blob); // 生成链接
					emlink.target = '_self';
					emlink.download = `${name}.xls`;
					document.body.appendChild(emlink);
					emlink.click();
					URL.revokeObjectURL(emlink.href); // 释放URL 对象
					document.body.removeChild(emlink);
				} else {
					navigator.msSaveBlob(blob);
				}
				resolve({ code: res.data.code, flag: true });
			})
			.catch(() => {
				resolve({ code: res.data.code, flag: false });
			});
	});
}
