import instance from './interceptor';
import { Message } from 'element-ui';
import qs from 'qs';
import router from '@/router';

/**
 * 核心函数，可通过它处理一切请求数据，并做横向扩展
 * @param {url} 请求地址
 * @param {params} 请求参数
 * @param {options} 请求配置，针对当前本次请求；
 * @param loading 是否显示loading
 * @param mock 本次是否请求mock而非线上
 * @param error 本次是否显示错误
 */
function request(
	url,
	params,
	options = {
		loading: true,
		mock: false,
		error: true,
	},
	method
) {
	let loadingInstance;
	// 请求前loading
	// if (options.loading) loadingInstance = Loading.service();
	return new Promise((resolve, reject) => {
		// post的参数
		let data = {
			data: params,
		};
		// get请求使用params字段
		if (method == 'get')
			data = {
				params,
			};
		// post请求使用data字段
		if (method == 'post')
			data = {
				...data,
			};
		// post请求使用query字段
		if (method == 'post-query')
			data = {
				...data,
				paramsSerializer: {
					serialize: function (params) {
						return qs.stringify(params, { arrayFormat: 'repeat' })
					}
				},
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				}
			};
		// post请求下载
		if (method == 'post-download')
			data = {
				...data,
				headers: {
					'Content-Type': 'application/json'
				},
				responseType: 'blob'
			};
		// get请求下载
		if (method == 'get-download')
			data = {
				params,
				paramsSerializer: {
					serialize: function (params) {
						return qs.stringify(params, { arrayFormat: 'repeat' })
					}
				},
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				},
				responseType: 'blob'
			};
		// put请求使用data字段
		if (method == 'put')
			data = {
				...data,
			};
		if (method == 'delete') {
			data = {
				params,
			};
		}
		//转换get
		if (method == 'get-download')
			method = 'get';

		//转换post
		if (method == 'post-query' || method == 'post-download')
			method = 'post';

		let cfg = {
			url, method, ...data
		};

		console.log(cfg, 'cfgcfgcfg');
		instance(cfg)
			.then((res) => {
				if (res.error) {
					// 通过配置可关闭错误提示
					if (options.error) Message.error(res.E_MSG);
					reject(res);

				} else {
					resolve(res);
				}
			})
			.catch((error) => {
				console.log('error', error);
			})
			.finally(() => { });
	});
}
// 封装GET请求
function get(url, params, options) {
	return request(url, params, options, 'get');
}
// 封装get-download请求
function getDownload(url, params, options) {
	return request(url, params, options, 'get-download');
}
// 封装POST请求
function post(url, params, options) {
	return request(url, params, options, 'post');
}
// 封装post-query请求
function postQuery(url, params, options) {
	return request(url, params, options, 'post-query');
}
// 封装post-download请求
function postDownload(url, params, options) {
	return request(url, params, options, 'post-download');
}
// 封装PUT请求
function put(url, params, options) {
	return request(url, params, options, 'put');
}

// 封装POST请求
function del(url, params, options) {
	return request(url, params, options, 'delete');
}
export default {
	get,
	getDownload,
	post,
	postQuery,
	postDownload,
	put,
	del,
};
