import axios from 'axios';
import { getLocalStorage, removeLocalStorage } from '@/utils/cache';
import { Message } from 'element-ui';
import router from '@/router';
// 登录中心
const loginCenter = process.env.LOGIN_CENTER;

const service = axios.create({
	headers: {
		post: {
			// "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
			'Content-Type': 'application/json',
		},
	},

	// baseURL: process.env.VUE_APP_BASE_URL ,
	//timeout: 10000,
	// withCredentials: true // 如果用的JSONP，可以配置此参数带上cookie凭证，如果是代理和CORS不用设置
});

// 请求拦截
service.interceptors.request.use((config) => {
	console.log('config--', config);

	// if (config.url.indexOf('/login') == -1) {
	// 	const token = getLocalStorage('X-Token');
	// 	if (token) {
	// 		config.headers['token'] = token;
	// 		return config;
	// 	} else {
	// 		if(loginCenter){
	// 			//window.location.href = loginCenter+new Date().getTime();
	// 		}else{
	// 			//window.location.href = '/#/login';
	// 		}

	// 	}
	// } else {
	// 	return config;
	// }
	return config;
});

// 返回拦截
service.interceptors.response.use(
	(response) => {
		console.log(response, '返回1122');
		const res = response.data;
		if (res.E_SUBRC === 'S') {
			return res;;
		}
		return res;
		// else if (res.code === 401) {
		// 	removeLocalStorage('X-Token');

		// 	// //重定向到登录中心 
		// 	// if(loginCenter){
		// 	// 	window.location.href = loginCenter+new Date().getTime();
		// 	// 	return;
		// 	// }else{
		// 	// 	router.push({
		// 	// 		path: '/login',
		// 	// 		query: { redirect: router.currentRoute.fullPath }, //从哪个页面跳转
		// 	// 	});
		// 	// }
		// 	return res;
		// }
	},
	(error) => {
		Message.error('网络请求异常，请稍后重试!');
		return Promise.reject(error);
	}
);
export default service;
