import request from '@/api/http/request';

/**我的项目列表 */
export function voteProjectList(params) {
	return request.get('/api/vote/project/list', params);
}
/**新建项目 */
export function voteProjectAdd(params) {
	return request.post('/api/vote/project/add', params);
}
/**新建项目表单list */
export function voteProjectItemList(params) {
	return request.get('/api/vote/project/item/list', params);
}
/**获取表单最大值id */
export function voteProjectItemMaxFormId(params) {
	return request.get('/api/vote/project/item/max-form-id', params);
}
/**修改模版描述标题 */
export function voteProjectUpdate(params) {
	return request.post('/api/vote/project/update', params);
}

/**复制模版 */
export function voteProjectCopy(params) {
	return request.post('/api/vote/project/copy', params);
}

/**新增投票表单项 */
export function voteProjectItemAdd(params) {
	return request.post('/api/vote/project/item/add', params);
}
/**修改投票表单项 */
export function voteProjectItemUpdate(params) {
	return request.post('/api/vote/project/item/update', params);
}
/**删除投票表单项 */
export function voteProjectItemDelete(params) {
	return request.post('/api/vote/project/item/delete', params);
}
/**保存模版 */
export function voteTemplateSave(params) {
	return request.post('/api/vote/template/add-by-project', params);
}
/**模版中心模版列表 */
export function voteTemplateList(params) {
	return request.get('/api/vote/template/list', params);
}
/**模版中心创建空模版 */
export function voteTemplateAdd(params) {
	return request.post('/api/vote/template/add', params);
}
/**模版中心删除模版 */
export function voteTemplateDelete(params) {
	return request.post('/api/vote/template/delete', params);
}
/**模版中心模版详情 */
export function voteTemplateItemList(params) {
	return request.get('/api/vote/template/item/list', params);
}
/**我的项目-删除我的项目 */
export function voteProjectDelete(params) {
	return request.post('/api/vote/project/delete', params);
}
/**我的项目-投票项目发布 */
export function voteProjectPublish(params) {
	return request.post('/api/vote/project/publish', params);
}
/**我的项目-记录点击量 */
export function voteReportView(params) {
	return request.post('/api/vote/report/view', params);
}
/**我的项目-保存投票接口 */
export function voteResultAdd(params) {
	return request.post('/api/vote/result/add', params);
}
/**我的项目-投票统计结果列表 */
export function voteResultList(params) {
	return request.get('/api/vote/result/list', params);
}
/**我的项目-投票统计收集信息 */
export function voteReportStats(params) {
	return request.get('/api/vote/report/stats', params);
}
/**我的项目-项目收集情况 */
export function voteReportSituation(params) {
	return request.get('/api/vote/report/situation', params);
}
/**我的项目-项目位置收集情况 */
export function voteReportPosition(params) {
	return request.get('/api/vote/report/position', params);
}
/**我的项目-项目设备收集情况 */
export function voteReportDevice(params) {
	return request.get('/api/vote/report/device', params);
}
/**我的项目-项目收集来源情况 */
export function voteReportSource(params) {
	return request.get('/api/vote/report/source', params);
}
/**我的项目-投票结果图表分析 */
export function voteReportAnalysis(params) {
	return request.get('/api/vote/report/analysis', params);
}
/**成员列表 */
export function employeeList(params) {
	return request.get('/api/employee/list', params);
}
/**公司列表 */
export function inwardCommonCompanyChoiceList(params) {
	return request.get('/api/inwardCommon/companyChoiceList', params);
}
