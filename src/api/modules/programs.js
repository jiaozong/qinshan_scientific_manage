import request from '@/api/http/request';

export const programsApi = {
	/**项目列表 */
	programsPageList(params) {
		return request.get('/train/programs/pageList', params);
	},

	/**创建项目 */
	programsSave(params) {
		return request.post('/train/programs/save', params);
	},

	/**项目详情 */
	findPrograms(params) {
		return request.get('/train/programs/find', params);
	},
	/**项目修改 */
	programsUpdate(params) {
		return request.post('/train/programs/update', params);
	},

	/**结束项目 */
	programsClose(params) {
		return request.post('/train/programs/close', params);
	},

	/**人员列表 */
	programsParticipantsList(params) {
		return request.get('/train/programsParticipants/pageList', params);
	},

	/**添加学员 */
	programsParticipantsAddStudents(params) {
		return request.post('/train/programsParticipants/addStudents', params);
	},

	/**移除学员 */
	programsParticipantsRemoveStudents(params) {
		return request.post('/train/programsParticipants/removeStudents', params);
	},

	/**通知报名 */
	programsParticipantsNoticeSignUp(params) {
		return request.post('/train/programsParticipants/noticeSignUp', params);
	},

	/**考试列表 */
	examPageList(params) {
		return request.get('/train/exam/pageList', params);
	},

	/**创建考试 */
	examSave(params) {
		return request.post('/train/exam/save', params);
	},

	/**考试详情 */
	examFind(params) {
		return request.get('/train/exam/find', params);
	},

	/**参考人员考试详情 */
	examinerDetail(params) {
		return request.get('/train/exam/examinerDetail', params);
	},

	/**考试修改 */
	examinerUpdate(params) {
		return request.post('/train/exam/update', params);
	},

	/**退回重考 */
	examinerRestExam(params) {
		return request.post('/train/exam/restExam', params);
	},

	/**评估列表 */
	appraisePageList(params) {
		return request.get('/train/appraise/pageList', params);
	},

	/**创建评估 */
	appraiseSave(params) {
		return request.post('/train/appraise/save', params);
	},

	/**评估详情 */
	appraiseFind(params) {
		return request.get('/train/appraise/find', params);
	},

	/**参评人员移除 */
	appraiseRemove(params) {
		return request.post('/train/appraise/remove', params);
	},

	/**评估修改 */
	appraiseUpdate(params) {
		return request.post('/train/appraise/update', params);
	},

	/**费用列表 */
	costPageList(params) {
		return request.get('/train/cost/pageList', params);
	},

	/**移除费用 */
	costDelete(params) {
		return request.del('/train/cost/delete', params);
	},

	/**新增费用 */
	costSave(params) {
		return request.post('/train/cost/save', params);
	},

	/**资料列表 */
	materialsPageList(params) {
		return request.get('/train/materials/pageList', params);
	},

	/**新增资料 */
	materialsSave(params) {
		return request.post('/train/materials/save', params);
	},

	/**移除资料 */
	materialsDelete(params) {
		return request.del('/train/materials/delete', params);
	},

	/**考勤管理列表 */
	attendancePageList(params) {
		return request.get('/train/attendance/pageList', params);
	},

	/**创建考勤 */
	attendanceSave(params) {
		return request.post('/train/attendance/save', params);
	},

	/**添加学员 */
	attendanceAddStudents(params) {
		return request.post('/train/attendance/addStudents', params);
	},

	/**学员签到情况列表 */
	attendanceStudentSignInPageList(params) {
		return request.get('/train/attendance/studentSignInPageList', params);
	},

	/**签到明细列表 */
	attendanceSignRecordPageList(params) {
		return request.get('/train/attendance/signRecordPageList', params);
	},

	/**考勤详情 */
	attendanceFind(params) {
		return request.get('/train/attendance/find', params);
	},

	/**移除学员 */
	attendanceRemoveStudents(params) {
		return request.post('/train/attendance/removeStudents', params);
	},

	/**获取考勤人员列表 */
	attendancePageListOfAttendPerson(params) {
		return request.get('/train/attendance/pageListOfAttendPerson', params);
	},

	/**考勤修改 */
	attendanceUpdate(params) {
		return request.post('/train/attendance/update', params);
	},
};
