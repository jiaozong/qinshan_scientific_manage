import request from '@/api/http/interceptor';

export function downloadIndex() {
	return request({
		url: '/api/idp/idpPlan/export',
		method: 'get',
		responseType: 'blob', // 声明返回blob
	});
}

export function exportExcelData(params) {
	return request({
		url: '/api/idp/manager/exportExcelData',
		method: 'post',
		params,
		responseType: 'blob', // 声明返回blob
	});
}

//盘点范围
export function downloadOtrrange(params) {
	return request({
		url: '/api/otr/otrrange/export',
		method: 'get',
		params,
		responseType: 'blob', // 声明返回blob
	});
}

//盘点范围结果（盘点报告中下载）
export function downloadOtrrangeResult(params) {
	return request({
		url: '/api/otr/otrrange/exportResult',
		method: 'get',
		params,
		responseType: 'blob', // 声明返回blob
	});
}

//下载人才模型模版
export function downloadTextarget() {
	return request({
		url: '/api/otr/otrtarget/export',
		method: 'get',
		responseType: 'blob', // 声明返回blob
	});
}

//下载上年数据模版
export function downloadOtrtargetdata(obj) {
	console.log('objobjobj', obj);
	return request({
		url: '/api/otr/otrtargetdata/export',
		method: 'get',
		params: obj,
		responseType: 'blob', // 声明返回blob
	});
}

//人才盘点-总结导入模板下载
export function downloadOtrsummaryempl(obj) {
	return request({
		url: '/api/otr/otrresult/excelExport',
		method: 'post',
		params: obj,
		responseType: 'blob', // 声明返回blob
	});
}

//整体盘点-总结导入模板下载
export function downloadOtrsummaryplan(obj) {
	return request({
		url: '/api/otr/otrplan/exportTemplate',
		method: 'get',
		params: obj,
		responseType: 'blob', // 声明返回blob
	});
}
//调研-模板下载
export function importModelDownload(params) {
	return request({
		url: '/api/research/researchReport/export',
		method: 'get',
		params,
		responseType: 'blob', // 声明返回blob
	});
}
//idp-下载人员关系模板
export function idpPlanExport(params) {
	return request({
		url: '/api/idp/idpPlan/export',
		method: 'get',
		params,
		responseType: 'blob', // 声明返回blob
	});
}
