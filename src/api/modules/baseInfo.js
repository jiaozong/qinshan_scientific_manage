import request from '@/api/http/request';

export const baseInfoApi = {
	//模拟登陆
	fetchLogin(params) {
		return request.post('/api/sso/login', params);
	},

	//获取公司列表
	getCompanyList(params) {
		return request.get('/common/pscompany/selectList', params);
	},

	//获取部门
	getDeptList(params) {
		return request.get('/common/psdepartment/selectList', params);
	},

	//基础数据列表 type= 1.课程/课程类别 2.培训-需求依据 3.培训-考核方法 4.讲师-等级 5.机构类型 6.计划类型 7.计划需求类型
	trainConstantSelectList(params) {
		return request.get('/common/trainConstant/selectList', params);
	},

	//获取用户数据
	getUserInfo(params) {
		return request.get('/api/ps/psjob/getCurrentUser', params);
	},

	//获取用户列表
	selectPerson(params) {
		return request.get('/api/ps/psjob/getEmpList', params);
	},

	//分页查询公司信息
	getCompanyInfoPage(params) {
		return request.get('/api/ps/psjob/page', params);
	},

	//通过用户ID获取信息
	getUserInfoById(params) {
		return request.get('/common/pspersion/getUserInfoById', params);
	},

	//获取岗位数据
	getPostList(params) {
		return request.get('/common/psmainpost/selectList', params);
	},

	//讲师列表
	getTeacherSelect(params) {
		return request.get('/common/trainTeacher/selectList', params);
	},

	//场地列表
	getTrainSiteSelect(params) {
		return request.get('/common/trainSite/selectList', params);
	},

	//课程列表
	getTrainCourseSelect(params) {
		return request.get('/common/trainCourse/selectList', params);
	},

	//培训机构机构列表
	getTrainInstitutionSelect(params) {
		return request.get('/common/trainInstitution/selectList', params);
	},

	//培训计划列表
	getTrainPlanSelect(params) {
		return request.get('/common/trainPlan/selectList', params);
	},

	//试卷模板列表
	getExamPaperSelect(params) {
		return request.get('/common/examPaper/selectList', params);
	},

	//计划筛选列表
	getTrainPlanPlanList(params) {
		return request.get('/common/trainPlan/planList', params);
	},

	//项目计划筛选列表
	getTrainProgramsPlanList(params) {
		return request.get('/common/trainPrograms/planList', params);
	},

	//------------------------------------------------xin------------------------------------------------

	//职序分页列表
	getListPosnSec(params) {
		return request.get('/api/common/listPosnSec', params);
	},

	//岗位分页列表
	getListMainPost(params) {
		return request.get('/api/common/listMainPost', params);
	},

	//职级分页列表
	getListPosnGrade(params) {
		return request.get('/api/common/listPosnGrade', params);
	},

	//职族分页列表
	getListProfession(params) {
		return request.get('/api/common/listProfession', params);
	},

	//字典数据分页列表
	getListDict(params) {
		return request.get('/api/common/listDict', params);
	},


	//根据公司ID获取组织节点
	getDeptTreeChildByCompany(params) {
		return request.get('/api/ps/psjob/getDeptTreeChildByCompany', params);
	},

	//获取组织树信息
	getDeptTreeAll(params) {
		return request.get('/api/ps/psjob/getDeptTreeAll', params);
	},
	getDeptTreeChild(params) {
		return request.get('/api/ps/psjob/getDeptTreeChild', params);
	},
	//分页查询员工信息
	getPsjobPageEmpInfo(params) {
		return request.get('/api/ps/psjob/pageEmpInfo', params);
	},


	//标准岗位-查询标准岗位信息(名称模糊查询)
	getDmposnsysListAll(params) {
		return request.get('/api/dm/dmposnsys/listAll', params);
	},
	//职级-查询职级信息
	getDmposngrdeListAll(params) {
		return request.get('/api/dm/dmposngrde/listAll', params);
	},
	//职序-查询所有序列
	getDmposnseqAll(params) {
		return request.get('/api/dm/dmposnseq/all', params);
	},
	//职族-查询职族(名称模糊查询)
	getDmposnsysListPosnGp(params) {
		return request.get('/api/dm/dmposnsys/listPosnGp', params);
	},

};
