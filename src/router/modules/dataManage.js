import index from '@/pages/layout/index.vue'; //布局

const dataManageRouter = {
    path: '/',
    name: 'dataManage',
    component: index,
    meta: { title: '数据管理', icon: 'iconfont icon-gongzuotai', affix: true },
    children: [
        {
            path: 'electronCertificate',
            name: 'electronCertificate',
            component: () => import('@/pages/dataManage/electronCertificate/index.vue'),
            meta: { title: '电子证照管理', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'dataImport',
            name: 'dataImport',
            component: () => import('@/pages/dataManage/dataImport/index.vue'),
            meta: { title: '数据导入', icon: 'iconfont icon-gongzuotai', affix: true },
        },
    ],
};

export default dataManageRouter