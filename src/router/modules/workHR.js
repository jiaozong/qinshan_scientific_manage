import index from '@/pages/layout/index.vue'; //布局

const workHRRouter = {
    path: '/',
    name: 'HR',
    component: index,
    meta: { title: 'HR工作台', icon: 'iconfont icon-gongzuotai', affix: true },
    children: [
        {
            path: 'workHR',
            name: 'workHR',
            component: () => import('@/pages/workHR/index.vue'),
            meta: { title: '首页', icon: 'iconfont icon-gongzuotai', affix: true },
        },

    ],
};

export default workHRRouter