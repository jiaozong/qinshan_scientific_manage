import index from '@/pages/layout/index.vue'; //布局

const staffSelfHelpRouter = {
    path: '/',
    name: 'staff',
    component: index,
    redirect: 'staffSelfHelp',
    meta: { title: '员工自助', icon: 'iconfont icon-gongzuotai', affix: true },
    children: [
        {
            path: 'staffSelfHelp',
            name: 'staffSelfHelp',
            component: () => import('@/pages/staffSelfHelp/index.vue'),
            meta: { title: '首页', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'checkWorkBoard',
            name: 'checkWorkBoard',
            component: () => import('@/pages/staffSelfHelp/components/checkWorkBoard.vue'),
            meta: { title: '考勤看板', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'leaveDetailed',
            name: 'leaveDetailed',
            component: () => import('@/pages/staffSelfHelp/components/leaveDetailed.vue'),
            meta: { title: '请假明细', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'timeoutClockDetailed',
            name: 'timeoutClockDetailed',
            component: () => import('@/pages/staffSelfHelp/components/timeoutClockDetailed.vue'),
            meta: { title: '超时打卡数据明细表', icon: 'iconfont icon-gongzuotai', affix: true },
        },
        {
            path: 'yearWorkDetailed',
            name: 'yearWorkDetailed',
            component: () => import('@/pages/staffSelfHelp/components/yearWorkDetailed.vue'),
            meta: { title: '年度工时统计对比表', icon: 'iconfont icon-gongzuotai', affix: true },
        },
    ],
};

export default staffSelfHelpRouter