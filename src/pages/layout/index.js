export { default as Header } from './Header.vue'
export { default as Aside } from './Aside/Aside.vue'
export { default as Main } from './Main.vue'