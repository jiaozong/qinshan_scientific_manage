export const USABLE = [
  {
    label: "是",
    value: "Y",
  },
  {
    label: "否",
    value: "N",
  },
];

export const STATUS = [
  {
    label: "在职",
    value: "A",
  },
  {
    label: "离职",
    value: "I",
  },
];
