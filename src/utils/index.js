export function toParams(params) {
	return window.btoa(encodeURIComponent(JSON.stringify({ params })));
}

export function fromParams(params) {
	return JSON.parse(decodeURIComponent(window.atob(params)));
}

//字符串截取
export function fromDay(params,index=0,num) {
	let str = params||'';
	return str.substring(index,num);
}

export const removeArray = (_arr, _obj) => {
	let length = _arr.length;
	if (!length) return [];
	for (var i = 0; i < length; i++) {
		if (_arr[i] == _obj) {
			if (i == 0) {
				_arr.shift();
				return _arr;
			} else if (i == length - 1) {
				_arr.pop();
				return _arr;
			} else {
				_arr.splice(i, 1);
				return _arr;
			}
		}
	}
};

/**
 * 防抖函数，返回函数连续调用时，空闲时间必须大于或等于 wait，func 才会执行
 *
 * @param  {function} func        回调函数
 * @param  {number}   wait        表示时间窗口的间隔
 * @param  {boolean}  immediate   设置为ture时，是否立即调用函数
 * @return {function}             返回客户调用函数
 */
export const debounce = (func, wait = 50, immediate = true) => {
	let timer, context, args;
	const later = () =>
		setTimeout(() => {
			timer = null;
			if (!immediate) {
				func.apply(context, args);
				context = args = null;
			}
		}, wait);

	return function (...params) {
		if (!timer) {
			timer = later();
			if (immediate) {
				func.apply(this, params);
			} else {
				context = this;
				args = params;
			}
		} else {
			clearTimeout(timer);
			timer = later();
		}
	};
};


/**
 * [isExternal description]
 * 判断是否是外链，如果是外链，则新开tab打开
 * @param  {[string]}  path [链接]
 * @return {Boolean}      [校验结果]
 */
export function isExternal(path) {
	return /^(https?:|mailto:|tel:)/.test(path);
}

//根据字段去重
export function unique(arr, u_key) {
	let map = new Map();
	arr.forEach((item, index) => {
		if (!map.has(item[u_key])) {
			map.set(item[u_key], item);
		}
	});
	return [...map.values()];
}

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export function parseTime(time, cFormat) {
	if (!time) {
		return '';
	}
	if (arguments.length === 0) {
		return null;
	}
	const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
	let date;
	if (typeof time === 'object') {
		date = time;
	} else {
		if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
			time = parseInt(time);
		}
		if (typeof time === 'number' && time.toString().length === 10) {
			time = time * 1000;
		}
		if (typeof time === 'number' && time.toString().length === 9) {
			time = time * 1000;
		}
		date = new Date(time);
	}
	const formatObj = {
		y: date != null ? date.getFullYear() : null,
		m: date != null ? date.getMonth() + 1 : null,
		d: date != null ? date.getDate() : null,
		h: date != null ? date.getHours() : null,
		i: date != null ? date.getMinutes() : null,
		s: date != null ? date.getSeconds() : null,
		a: date != null ? date.getDay() : null,
	};
	const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
		let value = formatObj[key];
		// Note: getDay() returns 0 on Sunday
		if (key === 'a') {
			return ['日', '一', '二', '三', '四', '五', '六'][value];
		}
		if (result.length > 0 && value < 10) {
			value = '0' + value;
		}
		return value || 0;
	});
	return time_str;
}

/***
 * @param time 13位时间戳 || date字符串
 * @param fmt
 * @returns {string}
 */
export function dateFormat(time, fmt = "yyyy-MM-dd hh:mm:ss") {
	if (!time) {
	  return "";
	}
  
	let date = new Date(time);
	if (/(y+)/.test(fmt)) {
	  fmt = fmt.replace(
		RegExp.$1,
		(date.getFullYear() + "").substr(4 - RegExp.$1.length)
	  );
	}
	let o = {
	  "M+": date.getMonth() + 1,
	  "d+": date.getDate(),
	  "h+": date.getHours(),
	  "m+": date.getMinutes(),
	  "s+": date.getSeconds(),
	};
	for (let k in o) {
	  if (new RegExp(`(${k})`).test(fmt)) {
		let str = o[k] + "";
		fmt = fmt.replace(
		  RegExp.$1,
		  RegExp.$1.length === 1 ? str : ("00" + str).substr(str.length)
		);
	  }
	}
	return fmt;
  }



  /**
 * 下载文件（通过url）
 * @param {String} url 文件流
 * @param {String} fileName 文件名称
 */
 export function downloadUrl(url, fileName) {
	let downloadElement = document.createElement("a");
	let href = url;
	downloadElement.href = href;
	downloadElement.download = fileName;
	document.body.appendChild(downloadElement);
	downloadElement.click();
	document.body.removeChild(downloadElement);
	window.URL.revokeObjectURL(href);
  }