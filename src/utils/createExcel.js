/**
 * @param {string} path
 * @returns {Boolean}
 */

export function createExcel(res, name) {
  let blob = new Blob([res], {
    type: "application/vnd.ms-excel",
  });
  let fileName = name; // 文件名+后缀
  // 允许用户在客户端上保存文件
  if (window.navigator.msSaveOrOpenBlob) {
    navigator.msSaveBlob(blob, fileName);
  } else {
    var link = document.createElement("a");//定义一个a标签
    link.href = window.URL.createObjectURL(blob);//需要生成一个 URL 来实现下载，链接到blob上
    link.download = fileName;//下载后的文件名称
    console.log("fileName", link);
    link.click(); //模拟在按钮上实现一次鼠标点击
    window.URL.revokeObjectURL(link.href); //释放 URL 对象
  }
}