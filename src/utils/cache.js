// 获取
export function getLocalStorage(Key) {
	return localStorage.getItem(Key);
}

// 设置
export function setLocalStorage(Key, value) {
	return localStorage.setItem(Key, value);
}

// 移除
export function removeLocalStorage(Key) {
	return localStorage.removeItem(Key);
}
